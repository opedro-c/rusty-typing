use uuid::Uuid;

use crate::models::Race;
use crate::models::User;
use crate::repository::RaceRepository;
use crate::repository::UserRepository;

#[derive(Debug)]
pub struct InputCreateRace {
    pub user: User,
    pub tokenized_text: Vec<String>,
}

pub struct CreateRace<'a> {
    pub user_repository: &'a mut dyn UserRepository,
    pub race_repository: &'a mut dyn RaceRepository,
}

impl CreateRace<'_> {
    pub fn execute(&mut self, input: InputCreateRace) -> Result<Race, &str> {
        let user = self
            .user_repository
            .find_user_by_nickname(input.user.nickname.clone());
        if user.is_none() {
            return Err("User does not exist");
        }
        let mut user = user.unwrap();
        let race_id = Uuid::new_v4();
        user.race_id = Some(race_id);
        let race = Race {
            id: race_id,
            tokenized_text: input.tokenized_text,
            started_at: None,
            finished_at: None,
        };
        self.user_repository.update_user(user);
        Ok(self.race_repository.create_race(race)?)
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;
    use std::sync::Mutex;

    use std::ops::DerefMut;

    use crate::use_cases::create_user::InputCreateUser;
    use crate::{
        mem_repository::{MemoryDatabase, RaceRepositoryMem, UserRepositoryMem},
        use_cases::create_user::CreateUser,
    };

    use super::*;

    struct TestContext {
        pub user_repository: Box<dyn UserRepository>,
        pub race_repository: Box<dyn RaceRepository>,
        pub created_user: User,
    }

    impl TestContext {
        fn setup() -> TestContext {
            let mem_database = Arc::new(Mutex::new(MemoryDatabase::new()));
            let mut user_repository = Box::new(UserRepositoryMem::new(mem_database.clone()));
            let created_user = CreateUser {
                user_repository: user_repository.deref_mut(),
            }
            .execute(InputCreateUser {
                user_nickname: "Pedro".to_string(),
            })
            .unwrap();
            TestContext {
                created_user,
                user_repository,
                race_repository: Box::new(RaceRepositoryMem::new(mem_database.clone())),
            }
        }
    }

    #[test]
    fn test_create_race_successfully() {
        let mut ctx = TestContext::setup();

        let mut create_race = CreateRace {
            user_repository: &mut *ctx.user_repository,
            race_repository: &mut *ctx.race_repository,
        };
        let race_created = create_race.execute(InputCreateRace {
            user: ctx.created_user.clone(),
            tokenized_text: Vec::from([
                "this".to_string(),
                "is".to_string(),
                "a".to_string(),
                "text.".to_string(),
            ]),
        });

        assert!(race_created.is_ok());

        let race_created = race_created.unwrap();
        let user_of_the_race = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone());
        let race_found = ctx.race_repository.find_race_by_id(race_created.id);

        assert_eq!(
            user_of_the_race.unwrap().race_id,
            Some(race_found.clone().unwrap().id)
        );
        assert!(race_found.is_some_and(|race| race.id.eq(&race_created.id)));
    }

    #[test]
    fn test_create_race_with_nonexistent_user() {
        let mut ctx = TestContext::setup();

        let mut create_race = CreateRace {
            user_repository: &mut *ctx.user_repository,
            race_repository: &mut *ctx.race_repository,
        };
        let race_created = create_race.execute(InputCreateRace {
            user: User {
                nickname: "nonexistent".to_string(),
                finished_at: None,
                typed_tokenized_text: Vec::new(),
                typing_mistakes: Vec::new(),
                race_id: None,
            },
            tokenized_text: Vec::from([
                "this".to_string(),
                "is".to_string(),
                "a".to_string(),
                "text.".to_string(),
            ]),
        });

        assert!(race_created.is_err());
    }
}
