use crate::models::User;
use crate::repository::UserRepository;

#[derive(Debug)]
pub struct InputCreateUser {
    pub user_nickname: String,
}

pub struct CreateUser<'a> {
    pub user_repository: &'a mut dyn UserRepository,
}

impl CreateUser<'_> {
    pub fn execute(&mut self, input: InputCreateUser) -> Result<User, &str> {
        if self
            .user_repository
            .find_user_by_nickname(input.user_nickname.clone())
            .is_some()
        {
            return Err("User already exists");
        }
        let user = User {
            nickname: input.user_nickname,
            finished_at: None,
            typed_tokenized_text: Vec::new(),
            typing_mistakes: Vec::new(),
            race_id: None,
        };
        Ok(self.user_repository.create_user(user)?)
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;
    use std::sync::Mutex;

    use crate::mem_repository::{MemoryDatabase, UserRepositoryMem};

    use super::*;

    struct TestContext {
        user_repository: Box<dyn UserRepository>,
    }

    impl TestContext {
        fn setup() -> TestContext {
            TestContext {
                user_repository: Box::new(UserRepositoryMem::new(
                    Arc::new(Mutex::new(MemoryDatabase::new())).clone(),
                )),
            }
        }
    }

    #[test]
    fn test_create_user_successfully() {
        let mut ctx = TestContext::setup();

        let mut create_user = CreateUser {
            user_repository: &mut *ctx.user_repository,
        };

        create_user
            .execute(InputCreateUser {
                user_nickname: "Pedro".to_string(),
            })
            .unwrap();

        let user_created = ctx
            .user_repository
            .find_user_by_nickname("Pedro".to_string())
            .unwrap();

        assert!(user_created.nickname.eq("Pedro"));
    }

    #[test]
    fn test_create_user_already_exists() {
        let mut ctx = TestContext::setup();

        let mut create_user = CreateUser {
            user_repository: &mut *ctx.user_repository,
        };

        create_user
            .execute(InputCreateUser {
                user_nickname: "Pedro".to_string(),
            })
            .unwrap();
        let res = create_user.execute(InputCreateUser {
            user_nickname: "Pedro".to_string(),
        });

        assert!(res.is_err());
    }
}
