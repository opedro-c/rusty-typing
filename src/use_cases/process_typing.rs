use std::time::Instant;

use crate::{
    models::{Race, User},
    repository::RaceRepository,
};
use uuid::Uuid;

use crate::{models::KeyEvent, repository::UserRepository};

#[derive(Debug)]
pub struct InputProcessTyping {
    pub user_nickname: String,
    pub race_id: Uuid,
    pub key_event: KeyEvent,
}

pub struct OutputProcessTyping {
    pub user: User,
    pub race_finished: bool,
    pub percentage_of_text_typed: f32,
    pub accuracy: f32,
    pub wpm: f32,
}

pub struct ProcessTyping<'a> {
    pub user_repository: &'a mut dyn UserRepository,
    pub race_repository: &'a mut dyn RaceRepository,
}

impl ProcessTyping<'_> {
    pub fn execute(&mut self, input: InputProcessTyping) -> Result<OutputProcessTyping, &str> {
        let mut race = self
            .race_repository
            .find_race_by_id(input.race_id)
            .ok_or("Race doesn't exist")?;
        if race.finished_at.is_some() {
            return Err("Race already finished!");
        }
        let mut user = self
            .user_repository
            .find_user_by_nickname(input.user_nickname)
            .ok_or("User doesn't exist")?;

        if user.finished_at.is_some() {
            return Err("User has already finished the race");
        }

        if race.started_at.is_none() {
            race.started_at = Some(Instant::now());
            self.race_repository.update_race(race.clone()).unwrap();
        }

        let word_to_be_compared = race
            .tokenized_text
            .get(if user.typed_tokenized_text.len() == 0 {
                0
            } else {
                user.typed_tokenized_text.len() - 1
            })
            .unwrap();

        match input.key_event {
            KeyEvent::Char(ch) => {
                match ch {
                    ' ' => {
                        if user.typed_tokenized_text.len() == race.tokenized_text.len() {
                            user.finished_at = Some(Instant::now());
                        } else {
                            user.typed_tokenized_text.push("".to_string());
                        }
                    }
                    c => {
                        self.concatenate_character_typed_to_user_tokenized_text(&mut user, c);
                        self.verify_typing_mistake(&mut user, word_to_be_compared);
                    }
                };
            }
            KeyEvent::Backspace => {
                let last_word_being_typed = user
                    .typed_tokenized_text
                    .pop()
                    .unwrap_or(String::new())
                    .clone();
                let last_word_being_typed_index = user.typed_tokenized_text.len();

                if last_word_being_typed.len() > 1 {
                    user.typed_tokenized_text.push(
                        last_word_being_typed[0..last_word_being_typed.len() - 1].to_string(),
                    );

                    self.remove_user_mistake_at_word_being_typed_index(
                        &mut user,
                        last_word_being_typed_index,
                        &last_word_being_typed,
                    );

                    self.user_repository.update_user(user.clone());
                } else if last_word_being_typed.len() == 1 {
                    user.typed_tokenized_text.push("".to_string());
                    self.remove_user_mistake_at_word_being_typed_index(
                        &mut user,
                        last_word_being_typed_index,
                        &last_word_being_typed,
                    );
                }
            }
            KeyEvent::Quit => todo!(),
        }
        self.user_repository.update_user(user.clone());

        let users_of_the_race = self.user_repository.find_users_by_race_id(race.id);
        let is_race_finished = users_of_the_race
            .iter()
            .all(|user| user.finished_at.is_some());
        if is_race_finished != race.finished_at.is_some() {
            race.finished_at = Some(Instant::now());
            self.race_repository.update_race(race.clone());
        }

        let output = OutputProcessTyping {
            user: user.clone(),
            percentage_of_text_typed: self
                .get_percentage_of_text_typed(&user.typed_tokenized_text, &race.tokenized_text),
            accuracy: self
                .get_accuraccy_of_text_typed(&user.typed_tokenized_text, &race.tokenized_text),
            race_finished: is_race_finished,
            wpm: self.get_wpm(&user, &race),
        };
        Ok(output)
    }

    fn get_percentage_of_text_typed(
        &self,
        user_text: &Vec<String>,
        race_text: &Vec<String>,
    ) -> f32 {
        let user_text_length = user_text.iter().fold(0, |acc, word| acc + word.len());
        let race_text_length = race_text.iter().fold(0, |acc, word| acc + word.len());

        user_text_length as f32 / race_text_length as f32
    }

    fn get_accuraccy_of_text_typed(&self, user_text: &Vec<String>, race_text: &Vec<String>) -> f32 {
        let mut total_chars = 0f32;
        let mut correct_chars = 0;

        for (user_word, race_word) in user_text.iter().zip(race_text.iter()) {
            for (u, r) in user_word.chars().zip(race_word.chars()) {
                total_chars += 1.0;
                if u == r {
                    correct_chars += 1;
                }
            }
        }

        correct_chars as f32 / total_chars
    }

    fn get_wpm(&self, user: &User, race: &Race) -> f32 {
        let words_typed = user.typed_tokenized_text.len();
        let time = Instant::now().duration_since(race.started_at.unwrap());
        let time_in_minutes = time.as_secs_f32() / 60.0;
        words_typed as f32 / time_in_minutes
    }

    fn concatenate_character_typed_to_user_tokenized_text(&self, user: &mut User, c: char) {
        let last_word_being_typed = user
            .typed_tokenized_text
            .pop()
            .unwrap_or(String::new())
            .clone();

        user.typed_tokenized_text
            .push(last_word_being_typed.clone() + &c.to_string().as_str());
    }

    fn verify_typing_mistake(&self, user: &mut User, word_to_be_compared: &String) {
        let last_word_being_typed = user.typed_tokenized_text.last().unwrap().clone();
        let last_word_being_typed_index = user.typed_tokenized_text.len() - 1;
        let last_character_typed_in_word_index = last_word_being_typed.len() - 1;

        if last_word_being_typed.len() <= word_to_be_compared.len() {
            let last_character_typed = last_word_being_typed.chars().last().unwrap();
            let character_to_be_compared = word_to_be_compared
                .chars()
                .nth(last_character_typed_in_word_index)
                .unwrap();
            if last_character_typed != character_to_be_compared {
                user.typing_mistakes.push([
                    last_word_being_typed_index,
                    last_character_typed_in_word_index,
                ]);
            }
        } else {
            user.typing_mistakes.push([
                last_word_being_typed_index,
                last_character_typed_in_word_index,
            ]);
        }
    }
    fn remove_user_mistake_at_word_being_typed_index(
        &self,
        user: &mut User,
        last_word_being_typed_index: usize,
        last_word_being_typed: &String,
    ) {
        user.typing_mistakes = user
            .typing_mistakes
            .iter()
            .filter(|mistake| {
                mistake[0] != last_word_being_typed_index
                    || mistake[1] != last_word_being_typed.len() - 1
            })
            .copied()
            .collect();
    }
}

#[cfg(test)]
mod tests {
    use crate::mem_repository::RaceRepositoryMem;
    use crate::mem_repository::UserRepositoryMem;
    use crate::models::Race;
    use crate::use_cases::create_race::InputCreateRace;
    use crate::use_cases::create_user::CreateUser;
    use crate::use_cases::create_user::InputCreateUser;
    use std::iter::zip;
    use std::sync::Mutex;
    use std::{ops::DerefMut, sync::Arc};

    use crate::{mem_repository::MemoryDatabase, use_cases::create_race::CreateRace};

    use super::*;

    struct TestContext {
        pub user_repository: Box<dyn UserRepository>,
        pub race_repository: Box<dyn RaceRepository>,
        pub created_user: User,
        pub created_race: Race,
        pub tokenized_text: Vec<String>,
    }

    impl TestContext {
        fn setup() -> TestContext {
            let mem_database = Arc::new(Mutex::new(MemoryDatabase::new()));
            let mut user_repository = Box::new(UserRepositoryMem::new(mem_database.clone()));
            let mut race_repository = Box::new(RaceRepositoryMem::new(mem_database.clone()));
            let created_user = CreateUser {
                user_repository: user_repository.deref_mut(),
            }
            .execute(InputCreateUser {
                user_nickname: "Pedro".to_string(),
            })
            .unwrap();
            let tokenized_text = vec![
                "Hi,".to_string(),
                "my".to_string(),
                "name".to_string(),
                "is".to_string(),
                "Pedro".to_string(),
            ];
            let created_race = CreateRace {
                user_repository: user_repository.deref_mut(),
                race_repository: race_repository.deref_mut(),
            }
            .execute(InputCreateRace {
                user: created_user.clone(),
                tokenized_text: tokenized_text.clone(),
            })
            .unwrap();
            TestContext {
                created_user,
                created_race,
                user_repository,
                race_repository,
                tokenized_text,
            }
        }
    }

    #[test]
    fn test_type_one_correct_character() {
        let mut ctx = TestContext::setup();

        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                race_id: ctx.created_race.id,
                key_event: KeyEvent::Char('H'),
            })
            .unwrap();

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(
            user.typed_tokenized_text.get(0).unwrap().get(0..).unwrap(),
            "H"
        );
    }

    #[test]
    fn test_type_one_correct_word() {
        let mut ctx = TestContext::setup();

        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let one_correct_word = "Hi, ".chars();
        for ch in one_correct_word {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(
            user.typed_tokenized_text.get(0).unwrap().get(0..).unwrap(),
            "Hi,"
        );
        assert_eq!(
            user.typed_tokenized_text.get(1).unwrap().get(0..).unwrap(),
            ""
        );
    }

    #[test]
    fn test_type_two_correct_words() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let two_correct_words = "Hi, my".chars();
        for ch in two_correct_words {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(
            user.typed_tokenized_text.get(0).unwrap().get(0..).unwrap(),
            "Hi,"
        );

        assert_eq!(
            user.typed_tokenized_text.get(1).unwrap().get(0..).unwrap(),
            "my"
        );
    }

    #[test]
    fn test_type_first_character_wrong() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                race_id: ctx.created_race.id,
                key_event: KeyEvent::Char('u'),
            })
            .unwrap();

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(*user.typing_mistakes.get(0).unwrap(), [0usize, 0usize]);
    }

    #[test]
    fn test_type_a_whole_word_wrong_with_correct_size() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let wrong_word = "uau";

        for ch in wrong_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(*user.typing_mistakes.get(0).unwrap(), [0usize, 0usize]);
        assert_eq!(*user.typing_mistakes.get(1).unwrap(), [0usize, 1usize]);
        assert_eq!(*user.typing_mistakes.get(2).unwrap(), [0usize, 2usize]);
    }

    #[test]
    fn test_type_a_whole_word_wrong_with_size_bigger_than_the_correct_word() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let wrong_word = "uaaaau";

        for ch in wrong_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        for i in 0..6 {
            assert_eq!(*user.typing_mistakes.get(i).unwrap(), [0usize, i]);
        }
    }

    #[test]
    fn test_type_two_words_wrong_with_correct_size() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let wrong_word = "uau we";

        for ch in wrong_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        for i in 0..3 {
            assert_eq!(*user.typing_mistakes.get(i).unwrap(), [0usize, i]);
        }

        for i in 3..5 {
            assert_eq!(*user.typing_mistakes.get(i).unwrap(), [1usize, i - 3]);
        }
    }

    #[test]
    fn test_type_two_words_wrong_with_bigger_sizes() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let wrong_word = "uaaau weee";

        for ch in wrong_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        for i in 0..5 {
            assert_eq!(*user.typing_mistakes.get(i).unwrap(), [0usize, i]);
        }

        for i in 5..9 {
            assert_eq!(*user.typing_mistakes.get(i).unwrap(), [1usize, i - 5]);
        }
    }

    #[test]
    fn test_type_two_words_wrong_with_smaller_sizes() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let wrong_word = "u w";

        for ch in wrong_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(*user.typing_mistakes.get(0).unwrap(), [0usize, 0usize]);
        assert_eq!(*user.typing_mistakes.get(1).unwrap(), [1usize, 0usize]);
    }

    #[test]
    fn test_type_partially_wrong_text() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let wrong_word = "Ji, mt";

        for ch in wrong_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    race_id: ctx.created_race.id,
                    key_event: KeyEvent::Char(ch),
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        dbg!(&user.typing_mistakes);

        assert_eq!(*user.typing_mistakes.get(0).unwrap(), [0usize, 0usize]);
        assert_eq!(*user.typing_mistakes.get(1).unwrap(), [1usize, 1usize]);
    }

    #[test]
    fn test_type_whole_text_correctly() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let correct_text = "Hi, my name is Pedro ";

        for ch in correct_text.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    key_event: KeyEvent::Char(ch),
                    race_id: ctx.created_race.id,
                })
                .unwrap();
        }

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();
        let race = ctx
            .race_repository
            .find_race_by_id(ctx.created_race.id)
            .unwrap();
        assert!(user.finished_at.is_some());
        assert!(race.finished_at.is_some());

        for (typed_word, correct_word) in zip(user.typed_tokenized_text, ctx.tokenized_text) {
            assert_eq!(typed_word, correct_word);
        }
    }

    #[test]
    fn test_type_a_correct_character_then_backspace() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Char('H'),
                race_id: ctx.created_race.id,
            })
            .unwrap();

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Backspace,
                race_id: ctx.created_race.id,
            })
            .unwrap();

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(user.typed_tokenized_text.first().unwrap(), "");
    }

    #[test]
    fn test_type_a_correct_word_then_backspace() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let typed_word = "Hi,";

        for ch in typed_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    key_event: KeyEvent::Char(ch),
                    race_id: ctx.created_race.id,
                })
                .unwrap();
        }

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Backspace,
                race_id: ctx.created_race.id,
            })
            .unwrap();

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(user.typed_tokenized_text.len(), 1);
        assert_eq!(user.typed_tokenized_text.first().unwrap(), "Hi");
    }

    #[test]
    fn test_type_a_correct_word_with_trailing_space_then_backspace() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let typed_word = "Hi, ";

        for ch in typed_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    key_event: KeyEvent::Char(ch),
                    race_id: ctx.created_race.id,
                })
                .unwrap();
        }

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Backspace,
                race_id: ctx.created_race.id,
            })
            .unwrap();

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(user.typed_tokenized_text.len(), 1);
        assert_eq!(user.typed_tokenized_text.first().unwrap(), "Hi,");
    }

    #[test]
    fn test_type_a_character_on_second_word_then_backspace() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let typed_word = "Hi, m";

        for ch in typed_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    key_event: KeyEvent::Char(ch),
                    race_id: ctx.created_race.id,
                })
                .unwrap();
        }

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Backspace,
                race_id: ctx.created_race.id,
            })
            .unwrap();

        let mut user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(user.typed_tokenized_text.len(), 2);
        assert_eq!(user.typed_tokenized_text.pop().unwrap(), "");
        assert_eq!(user.typed_tokenized_text.pop().unwrap(), "Hi,");
    }

    #[test]
    fn test_type_a_character_on_second_word_then_backspace_then_type_another_character() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        let typed_word = "Hi, m";

        for ch in typed_word.chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    key_event: KeyEvent::Char(ch),
                    race_id: ctx.created_race.id,
                })
                .unwrap();
        }

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Backspace,
                race_id: ctx.created_race.id,
            })
            .unwrap();

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Char('y'),
                race_id: ctx.created_race.id,
            })
            .unwrap();

        let mut user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(user.typed_tokenized_text.len(), 2);
        assert_eq!(user.typed_tokenized_text.pop().unwrap(), "y");
        assert_eq!(user.typed_tokenized_text.pop().unwrap(), "Hi,");
    }

    #[test]
    fn test_delete_a_wrong_character() {
        let mut ctx = TestContext::setup();
        let mut process_typing = ProcessTyping {
            user_repository: ctx.user_repository.deref_mut(),
            race_repository: ctx.race_repository.deref_mut(),
        };

        for ch in "Ji, st".chars() {
            process_typing
                .execute(InputProcessTyping {
                    user_nickname: ctx.created_user.nickname.clone(),
                    key_event: KeyEvent::Char(ch),
                    race_id: ctx.created_race.id,
                })
                .unwrap();
        }

        process_typing
            .execute(InputProcessTyping {
                user_nickname: ctx.created_user.nickname.clone(),
                key_event: KeyEvent::Backspace,
                race_id: ctx.created_race.id,
            })
            .unwrap();

        let user = ctx
            .user_repository
            .find_user_by_nickname(ctx.created_user.nickname.clone())
            .unwrap();

        assert_eq!(user.typed_tokenized_text.len(), 2);
        let mut iter = user.typing_mistakes.iter();
        assert_eq!(iter.next().unwrap(), &[0usize, 0usize]);
        assert_eq!(iter.next().unwrap(), &[1usize, 0usize]);
        assert!(user.typing_mistakes.len() == 2);
    }
}
