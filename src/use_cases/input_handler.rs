use crate::models::KeyEvent;
use std::fs::read_to_string;
use std::io::Stdin;
use std::str::FromStr;
use std::sync::mpsc::Sender;

use termion::input::TermRead;

use crate::{models::Difficulty, models::Settings};

pub struct InputHandler;

impl InputHandler {
    pub fn handle_input(sender: Sender<KeyEvent>, stdin: Stdin) {
        for c in stdin.lock().keys() {
            let _ = match c.unwrap() {
                termion::event::Key::Backspace => sender.send(KeyEvent::Backspace),
                termion::event::Key::Char(ch) => sender.send(KeyEvent::Char(ch)),
                termion::event::Key::Ctrl('c') => sender.send(KeyEvent::Quit),
                _ => Ok(()),
            };
        }
    }

    pub fn parse_args(args: Vec<String>) -> Result<Settings, String> {
        let mut args = args.into_iter();
        let mut difficulty: Difficulty = Difficulty::Medium;
        let mut text: Option<String> = None;
        let mut name = "anonymous".to_string();

        while let Some(arg) = args.next() {
            if arg.eq("-d") {
                difficulty = match args.next() {
                    Some(another_arg) => match Difficulty::from_str(another_arg.as_str()) {
                        Ok(expr) => expr,
                        Err(_) => return Err("Invalid difficulty!".to_string()),
                    },
                    None => return Err("Missing difficulty!".to_string()),
                }
            }

            if arg.eq("-t") {
                text = match read_to_string(match args.next() {
                    Some(arg) => arg,
                    None => return Err("Missing text file path!".to_string()),
                }) {
                    Ok(content) => Some(content),
                    Err(_) => return Err("Could not read file!".to_string()),
                }
            }

            if arg.eq("-n") {
                name = args.next().ok_or("Could not read name!")?;
            }
        }

        Ok(Settings {
            difficulty,
            text,
            name,
        })
    }
}

// ================== TESTS ==================

#[cfg(test)]
mod tests {
    use std::iter::zip;

    use super::*;

    #[test]
    fn test_parse_args_with_difficulty() {
        let args = vec![
            vec!["-d".to_string(), "easy".to_string()],
            vec!["-d".to_string(), "medium".to_string()],
            vec!["-d".to_string(), "hard".to_string()],
        ];

        let expected_settings = vec![
            Settings {
                difficulty: Difficulty::Easy,
                text: None,
                name: "anonymous".to_string(),
            },
            Settings {
                difficulty: Difficulty::Medium,
                text: None,
                name: "anonymous".to_string(),
            },
            Settings {
                difficulty: Difficulty::Hard,
                text: None,
                name: "anonymous".to_string(),
            },
        ];

        for (arg, expected_setting) in zip(args, expected_settings) {
            assert_eq!(InputHandler::parse_args(arg), Ok(expected_setting));
        }
    }

    #[test]
    fn test_parse_args_with_text() {
        let args = vec!["-t".to_string(), "assets/test_text.txt".to_string()];
        let expected_settings = Settings {
            difficulty: Difficulty::Medium,
            text: Some("Test Text\n".to_string()),
            name: "anonymous".to_string(),
        };

        assert_eq!(InputHandler::parse_args(args), Ok(expected_settings));
    }

    #[test]
    fn test_parse_args_without_difficulty() {
        let args = vec!["-d".to_string()];

        assert!(InputHandler::parse_args(args).is_err());
    }

    #[test]
    fn test_parse_args_with_invalid_difficulty() {
        let args = vec!["-d".to_string(), "uau".to_string()];

        assert!(InputHandler::parse_args(args).is_err());
    }

    #[test]
    fn test_parse_args_without_text() {
        let args = vec!["-t".to_string()];

        assert!(InputHandler::parse_args(args).is_err());
    }

    #[test]
    fn test_parse_args_with_invalid_file_path() {
        let args = vec!["-t".to_string(), "uau".to_string()];

        assert!(InputHandler::parse_args(args).is_err());
    }

    #[test]
    fn test_parse_args_with_name() {
        let args = vec!["-n".to_string(), "Pedro".to_string()];
        let expected_settings = Settings {
            difficulty: Difficulty::Medium,
            text: None,
            name: "Pedro".to_string(),
        };

        assert_eq!(InputHandler::parse_args(args), Ok(expected_settings));
    }
}
