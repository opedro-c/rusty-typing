use std::time::Instant;

use strum_macros::{Display, EnumString};
use uuid::Uuid;

#[derive(Debug, Display, EnumString, Clone, Copy, PartialEq)]
pub enum Difficulty {
    #[strum(serialize = "easy")]
    Easy,
    #[strum(serialize = "medium")]
    Medium,
    #[strum(serialize = "hard")]
    Hard,
}

#[derive(Debug)]
pub enum KeyEvent {
    Char(char),
    Backspace,
    Quit,
}

#[derive(Debug, Clone)]
pub struct User {
    pub nickname: String,
    pub finished_at: Option<Instant>,
    pub typed_tokenized_text: Vec<String>,
    pub typing_mistakes: Vec<[usize; 2]>,
    pub race_id: Option<Uuid>,
}

#[derive(Debug, Clone)]
pub struct Race {
    pub id: Uuid,
    pub tokenized_text: Vec<String>,
    pub started_at: Option<Instant>,
    pub finished_at: Option<Instant>,
}

#[derive(Debug, PartialEq)]
pub struct Settings {
    pub difficulty: Difficulty,
    pub text: Option<String>,
    pub name: String,
}
