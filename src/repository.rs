use uuid::Uuid;

use crate::models::{Race, User};

pub trait UserRepository {
    fn find_user_by_nickname(&self, nickname: String) -> Option<User>;
    fn create_user(&mut self, user: User) -> Result<User, &str>;
    fn update_user(&mut self, user: User) -> Option<User>;
    fn find_users_by_race_id(&self, race_id: Uuid) -> Vec<User>;
}

pub trait RaceRepository {
    fn create_race(&mut self, race: Race) -> Result<Race, &str>;
    fn find_race_by_id(&mut self, race_id: Uuid) -> Option<Race>;
    fn update_race(&mut self, race: Race) -> Option<Race>;
}
