use std::sync::{Arc, Mutex};

use uuid::Uuid;

use crate::{
    models::{Race, User},
    repository::{RaceRepository, UserRepository},
};

#[derive(Debug)]
pub struct MemoryDatabase {
    pub users: Vec<User>,
    pub races: Vec<Race>,
}

impl MemoryDatabase {
    pub fn new() -> MemoryDatabase {
        MemoryDatabase {
            users: Vec::new(),
            races: Vec::new(),
        }
    }
}

pub struct UserRepositoryMem {
    connection_database: Arc<Mutex<MemoryDatabase>>,
}

impl UserRepository for UserRepositoryMem {
    fn find_user_by_nickname(&self, nickname: String) -> Option<User> {
        self.connection_database
            .lock()
            .unwrap()
            .users
            .iter()
            .find(|user| user.nickname == nickname)
            .cloned()
    }

    fn create_user(&mut self, user: User) -> Result<User, &str> {
        let connection_database_result = self.connection_database.lock();
        if let Ok(mut connection_database) = connection_database_result {
            connection_database.users.push(user.clone());
            return Ok(user);
        }
        Err("Database connection error")
    }

    fn update_user(&mut self, user: User) -> Option<User> {
        let user_position = self
            .connection_database
            .lock()
            .unwrap()
            .users
            .iter()
            .position(|_user| _user.nickname == user.nickname);

        if user_position.is_some() {
            self.connection_database.lock().unwrap().users[user_position.unwrap()] = user.clone();
        }
        Some(user)
    }
    fn find_users_by_race_id(&self, race_id: Uuid) -> Vec<User> {
        self.connection_database
            .lock()
            .unwrap()
            .users
            .iter()
            .filter(|user| user.race_id == Some(race_id))
            .cloned()
            .collect()
    }
}

impl UserRepositoryMem {
    pub fn new(connection_database: Arc<Mutex<MemoryDatabase>>) -> UserRepositoryMem {
        UserRepositoryMem {
            connection_database,
        }
    }
}

#[derive(Debug)]
pub struct RaceRepositoryMem {
    connection_database: Arc<Mutex<MemoryDatabase>>,
}

impl RaceRepository for RaceRepositoryMem {
    fn create_race(&mut self, race: Race) -> Result<Race, &str> {
        let connection_database_result = self.connection_database.lock();
        if let Ok(mut connection_database) = connection_database_result {
            connection_database.races.push(race.clone());
            return Ok(race);
        }
        Err("Database connection error")
    }

    fn find_race_by_id(&mut self, race_id: Uuid) -> Option<Race> {
        self.connection_database
            .lock()
            .unwrap()
            .races
            .iter()
            .find(|race| race.id == race_id)
            .cloned()
    }

    fn update_race(&mut self, race: Race) -> Option<Race> {
        let race_position = self
            .connection_database
            .lock()
            .unwrap()
            .races
            .iter()
            .position(|_race| _race.id == race.id);

        if race_position.is_some() {
            self.connection_database.lock().unwrap().races[race_position.unwrap()] = race.clone();
        }
        Some(race)
    }
}

impl RaceRepositoryMem {
    pub fn new(connection_database: Arc<Mutex<MemoryDatabase>>) -> RaceRepositoryMem {
        RaceRepositoryMem {
            connection_database,
        }
    }
}
