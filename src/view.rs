use colored::Colorize;
use std::io::{Stdout, Write};
use termion::{raw::RawTerminal, screen::AlternateScreen};

pub struct View<'a> {
    screen: &'a mut AlternateScreen<RawTerminal<Stdout>>,
}

impl<'a> View<'a> {
    pub fn new(screen: &'a mut AlternateScreen<RawTerminal<Stdout>>) -> Self {
        View { screen }
    }

    pub fn clear_screen(&mut self) {
        write!(self.screen, "{}", termion::clear::All).unwrap();
    }

    pub fn flush(&mut self) {
        self.screen.flush().unwrap();
    }

    pub fn write(&mut self, text: &str) {
        write!(self.screen, "{}", text.white()).unwrap();
    }

    pub fn write_text_with_colors(
        &mut self,
        user_tokenized_text: Vec<String>,
        mistakes: Vec<[usize; 2]>,
    ) {
        for i in 0..user_tokenized_text.len() {
            let word = &user_tokenized_text[i].chars();
            for (j, ch) in word.clone().enumerate() {
                let is_mistake = mistakes.iter().any(|[a, b]| i == *a && j == *b);
                if is_mistake {
                    write!(self.screen, "{}", ch.to_string().red()).unwrap();
                } else {
                    write!(self.screen, "{}", ch.to_string().green()).unwrap();
                }
            }
            if i < user_tokenized_text.len() - 1 {
                write!(self.screen, " ").unwrap();
            }
        }
    }

    pub fn move_cursor_to(&mut self, x: u16, y: u16) {
        write!(self.screen, "{}", termion::cursor::Goto(x, y)).unwrap();
    }
}
