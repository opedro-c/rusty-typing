use crate::use_cases::input_handler::InputHandler;
use mem_repository::MemoryDatabase;
use mem_repository::RaceRepositoryMem;
use mem_repository::UserRepositoryMem;
use models::KeyEvent;
use std::env;
use std::io::stdin;
use std::io::stdout;
use std::ops::DerefMut;
use std::sync::mpsc::channel;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread::spawn;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;
use use_cases::create_race::CreateRace;
use use_cases::create_race::InputCreateRace;
use use_cases::create_user::CreateUser;
use use_cases::create_user::InputCreateUser;
use use_cases::process_typing::InputProcessTyping;
use use_cases::process_typing::ProcessTyping;
use view::View;

pub mod mem_repository;
pub mod models;
pub mod repository;
pub mod use_cases;
pub mod view;

fn main() {
    let args: Vec<String> = env::args().collect();
    let settings = InputHandler::parse_args(args).unwrap();
    let memory_database = Arc::new(Mutex::new(MemoryDatabase::new()));
    let mut user_repository = Box::new(UserRepositoryMem::new(memory_database.clone()));
    let mut race_repository = Box::new(RaceRepositoryMem::new(memory_database));
    let (sender, receiver) = channel::<KeyEvent>();

    let user = CreateUser {
        user_repository: user_repository.deref_mut(),
    }
    .execute(InputCreateUser {
        user_nickname: settings.name,
    })
    .unwrap();

    let race_text = settings.text.unwrap();
    let race_tokenized_text = race_text.split(' ').map(|s| s.to_string()).collect();
    let race = CreateRace {
        user_repository: user_repository.deref_mut(),
        race_repository: race_repository.deref_mut(),
    }
    .execute(InputCreateRace {
        user: user.clone(),
        tokenized_text: race_tokenized_text,
    })
    .unwrap();

    let mut process_typing = ProcessTyping {
        user_repository: user_repository.deref_mut(),
        race_repository: race_repository.deref_mut(),
    };

    let stdout = stdout().into_raw_mode().unwrap();
    let mut screen = AlternateScreen::from(stdout);

    spawn(move || {
        InputHandler::handle_input(sender, stdin());
    });

    let mut view = View::new(&mut screen);

    view.move_cursor_to(1, 1);
    view.clear_screen();
    view.write(&format!("Percentage of text typed: {}%", 0f32));
    view.move_cursor_to(1, 2);
    view.write(&format!("Accuracy: {}%", 100f32));
    view.move_cursor_to(1, 3);
    view.write(&format!("WPM: {}", 0));
    view.move_cursor_to(1, 5);
    view.write(race_text.as_str());
    view.move_cursor_to(1, 7);
    view.flush();

    loop {
        let output = process_typing
            .execute(InputProcessTyping {
                user_nickname: user.nickname.clone(),
                race_id: race.id,
                key_event: receiver.recv().unwrap(),
            })
            .unwrap();
        view.move_cursor_to(1, 1);
        view.clear_screen();

        view.write(&format!(
            "Percentage of text typed: {}%",
            output.percentage_of_text_typed * 100f32
        ));
        view.move_cursor_to(1, 2);
        view.write(&format!("Accuracy: {}%", output.accuracy * 100f32));
        view.move_cursor_to(1, 3);
        view.write(&format!("WPM: {}", output.wpm));
        view.move_cursor_to(1, 5);
        view.write(race_text.as_str());
        view.move_cursor_to(1, 7);
        view.write_text_with_colors(
            output.user.typed_tokenized_text,
            output.user.typing_mistakes,
        );
        view.flush();

        if output.race_finished {
            break;
        }
    }

    // drop(screen)
}
