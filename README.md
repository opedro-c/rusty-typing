# Rusty Typing

![License](https://img.shields.io/gitlab/license/58626426?style=for-the-badge)
![Language count](https://img.shields.io/gitlab/languages/count/opedro-c/rusty-typing?style=for-the-badge)
![Forks](https://img.shields.io/gitlab/forks/opedro-c/rusty-typing?style=for-the-badge)
![Open issues](https://img.shields.io/gitlab/issues/open/58626426?style=for-the-badge)
![Merge Requests](https://img.shields.io/gitlab/merge-requests/open/58626426?style=for-the-badge)


<img src="assets/demo.gif" alt="A gif showing a terminal with a text being typed while some information updates automatically">

That program aims to provide a system that calculate in real time:

- Which characters the user typed correctly and incorrectly
- Which percentage of the text was already typed
- What's the accuracy of the text typed
- How many words per minute the user is able to type

### Next steps

The project is still in development, in the next updates the new features are going to be:

- [ ] Save race history
- [ ] Host a race for multiple users

## 💻 Prerequisites

Before we begin, you need to make sure that:

- You are running the latest version of Rust Language
- You are running a gnu+linux operating system.

## ☕ Running Rusty-Typing

To run rusty-typing, enter the following command on your terminal:

```
rusty-typing -t <path to the text file> -n <nickname>
```

## 🤝 Maintainer

- [Pedro Costa Aragão](https://gitlab.com/opedro-c)

## 📝 License

This project is under a license. Check the file [LICENSE](LICENSE) for more details.
